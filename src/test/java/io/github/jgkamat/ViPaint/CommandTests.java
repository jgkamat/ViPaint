package io.github.jgkamat.ViPaint;

import io.github.jgkamat.ViPaint.Canvas.CursorCanvas;
import io.github.jgkamat.ViPaint.Handlers.SettingManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import io.github.jgkamat.ViPaint.VimBar.CommandManager;
import io.github.jgkamat.ViPaint.VimBar.VimBar;
import javafx.scene.paint.Color;
import org.junit.Test;

import java.util.Random;

/**
 * A test class to test various commands
 */
public class CommandTests {

    /**
     * A test that will check to see if the setting manager is working properly
     */
    @Test
    public void testColorSwitch() {
        CommandManager manager = new CommandManager(null, null, null, null);
        Color def = (Color) SettingManager.get("color");
        manager.parseCommand("let color=SKYBLUE");
        assertEquals(Color.SKYBLUE, SettingManager.get("color"));

        manager.parseCommand("let color = MAROON");
        assertEquals(Color.MAROON, SettingManager.get("color"));

        manager.parseCommand("let color   = MAGENTA");
        assertEquals(Color.MAGENTA, SettingManager.get("color"));
    }


    /**
     * A test that will test if ints are being stored correctly
     */
    @Test
    public void testIntSwitch() {
        CommandManager manager = new CommandManager(null, null, null, null);

        SettingManager.entrySet().stream().filter((keyVal) -> keyVal.getValue() instanceof Integer)
            .forEach((keyVal) -> testIntHelper(keyVal.getKey(), manager));
    }

    /**
     * A helper method to test if int values are being changed
     * @param key Key to test
     * @param manager CommandManager to use parsing with
     */
    private void testIntHelper(String key, CommandManager manager) {
        Random r = new Random();
        int toStore = r.nextInt(999999);

        // Don't select the same value
        if (toStore == (int) SettingManager.get(key)) {
            testIntHelper(key, manager);
            return;
        }

        manager.parseCommand("let " + key + "=" + toStore);
        assertEquals(toStore, (int) SettingManager.get(key));

        manager.parseCommand("let " + key + " = " + (toStore + 2));
        assertEquals(toStore + 2, (int) SettingManager.get(key));

        manager.parseCommand("let " + key + "    = " + (toStore + 5));
        assertEquals(toStore + 5, (int) SettingManager.get(key));

        // Same value as last time
        manager.parseCommand("let " + key + "    = " + (toStore + 5));
        assertEquals(toStore + 5, (int) SettingManager.get(key));
    }
}
