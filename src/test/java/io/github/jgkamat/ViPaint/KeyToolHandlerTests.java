package io.github.jgkamat.ViPaint;

import static org.junit.Assert.*;

import io.github.jgkamat.ViPaint.Handlers.KeyToolHandler;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.*;
import org.junit.Test;

/**
 * A test class to test the keytool manager
 */
public class KeyToolHandlerTests {

    /**
     * A test that will check to see if the keytool manager is working properly
     */
    @Test
    public void testKeyToolHandler() {
        KeyToolHandler ktool = new KeyToolHandler();

        assertFalse(ktool.containsToolName("Normal"));

        // TODO Come up with a reflection solution for this
        Line toSet = new Line();
        assertFalse(ktool.containsToolName("Normal"));
        ktool.addTool(new Normal());
        ktool.addTool(new Insert());
        ktool.addTool(new Delete());
        ktool.addTool(new Rectangle());
        ktool.addTool(toSet);
        ktool.addTool(new Oval());

        assertTrue(ktool.containsToolName("Normal"));
        ktool.setTool("Normal");
        assertTrue(ktool.getCurrentTool() instanceof Normal);

        ktool.setTool(toSet);
        assertTrue(ktool.getCurrentTool() instanceof Line);
    }
}
