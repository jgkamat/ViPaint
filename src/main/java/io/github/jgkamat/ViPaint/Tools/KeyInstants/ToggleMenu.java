package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Handlers.SetVariable;
import io.github.jgkamat.ViPaint.Handlers.SettingManager;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * Show/Hide the Menu
 *
 * @author Matthew Keezer
 * @version 1.0
 */
public class ToggleMenu implements KeyInstant {
    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        // Heh why not?
        if (repetitions % 2 == 1) {
            ((SetVariable) SettingManager.get("graphical")).toggle();
        }
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.TAB;
    }

    @Override
    public String getName() {
        return "ToggleMenu";
    }

    @Override
    public String getHelp() {
        return "The Toggle Menu tool will quickly Show or Hide the Tool Menu.";
    }

}
