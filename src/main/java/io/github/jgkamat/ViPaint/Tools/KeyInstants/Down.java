package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Handlers.SettingManager;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

import java.util.HashSet;
import java.util.Set;

/**
 * A Down tool
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Down implements KeyInstant {

    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        c.moveDown(((int) SettingManager.get("move_distance")) * repetitions);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.J;
    }

    @Override
    public Set<KeyCode> getDefaultKeys() {
        HashSet<KeyCode> ans = new HashSet<>();
        ans.add(getDefaultKey());
        ans.add(KeyCode.DOWN);
        return ans;
    }

    @Override
    public String getName() {
        return "Down";
    }

    @Override
    public String getHelp() {
        return "The down instant tool will move the cursor down by 'move_amount'";
    }

}
