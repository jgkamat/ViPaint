package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * A tool that is just used to move around! (an empty class that does nothing)
 * @author Jay Kamat
 * @version 1.0
 */
public class Normal implements KeyModeTool {
    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        return;
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        return;
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        return;
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.ESCAPE;
    }

    @Override
    public String getName() {
        return "Normal";
    }

    @Override
    public boolean hasPreview() {
        return false;
    }
}
