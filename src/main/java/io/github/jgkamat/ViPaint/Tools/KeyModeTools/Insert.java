package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * A insert tool that behaves like a paintbrush
 * @author Jay Kamat
 * @version 1.0
 */
public class Insert implements KeyModeTool {

    private double startx, starty;

    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        startx = c.getX();
        starty = c.getY();

        onMoveInMode(c, e, g);
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        g.fillRect(Math.min(startx, c.getX()),
                Math.min(starty, c.getY()),
                Math.max(startx + c.getWidth() - c.getX(), c.getX() +
                    c.getWidth() - startx),
                Math.max(starty + c.getHeight() - c.getY(), c.getY() +
                    c.getHeight() - starty));

        startx = c.getX();
        starty = c.getY();
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        onMoveInMode(c, e, g);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.I;
    }

    @Override
    public String getName() {
        return "Insert";
    }

    @Override
    public String getHelp() {
        return "The insert tool is used for simple painting.\n\n" +
                "Press I or i to activate the tool, move around\n" +
                "with h,j,k, and l, and press <ESC> to complete the drawing.";
    }

    @Override
    public boolean hasPreview() {
        return false;
    }
}
