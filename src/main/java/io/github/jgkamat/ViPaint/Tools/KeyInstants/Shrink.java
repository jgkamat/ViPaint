package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

import java.util.HashSet;
import java.util.Set;

/**
 * A Shrinking tool
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Shrink implements KeyInstant {

    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        c.shrink(1 * repetitions);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.SUBTRACT;
    }

    @Override
    public Set<KeyCode> getDefaultKeys() {
        HashSet<KeyCode> ans = new HashSet<>();
        ans.add(getDefaultKey());
        ans.add(KeyCode.UNDERSCORE);
        ans.add(KeyCode.MINUS);
        return ans;
    }

    @Override
    public String getName() {
        return "Shrink";
    }

    @Override
    public String getHelp() {
        return "The Shrink instant tool will shrink the cursor";
    }

}
