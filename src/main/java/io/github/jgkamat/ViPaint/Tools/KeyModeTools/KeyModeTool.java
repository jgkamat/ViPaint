package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import io.github.jgkamat.ViPaint.Tools.KeyTool;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * This interface represents a KeyModeTool object.
 * KeyModeTools are modal tools.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public interface KeyModeTool extends KeyTool {

    /**
     * Tools method that is called when the first key is pressed.
     * Usually involves beginning to draw something.
     *
     * @param c The cursor that is passed in
     * @param g The current graphics context.
     */
    void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g);

    /**
     * Tools method that is called when the cursor is moved.
     * Usually involves updating the drawing location.
     *
     * @param c The cursor that is passed in
     * @param g The current graphics context.
     */
    void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g);

    /**
     * Tools method that is called when the cursor leaves the current mode.
     * Usually involves completing an individual stroke/shape.
     *
     * @param c The cursor that is passed in
     * @param g The current graphics context.
     */
    void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g);

    /**
     * The keycode of this tool.
     *
     * @return This tool's VK value
     */
    KeyCode getDefaultKey();

    /**
     * The name of the string
     *
     * @return the tools name
     */
    String getName();

    /**
     * Gets the help text for the specified class
     * @return The help text
     */
    default String getHelp() {
        return "The help text for this class has not been created yet!";
    }

    /**
     * Does this tool have a preview thingy
     *
     * @return Preview during 'drag'
     */
    boolean hasPreview();
}
