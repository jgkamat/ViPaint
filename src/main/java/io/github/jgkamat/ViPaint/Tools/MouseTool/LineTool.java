package io.github.jgkamat.ViPaint.Tools.MouseTool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 * A Line tool!
 * Thank god I dont need to checkstle this too much
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class LineTool implements MouseTool {

    private double startx, starty;

    @Override
    public void onPress(MouseEvent e, GraphicsContext g) {
        startx = e.getX();
        starty = e.getY();
    }

    @Override
    public void onDrag(MouseEvent e, GraphicsContext g) {
        g.strokeLine(startx, starty, e.getX(), e.getY());
    }

    @Override
    public void onRelease(MouseEvent e, GraphicsContext g) {
        g.strokeLine(startx, starty, e.getX(), e.getY());
    }

    @Override
    public String getName() {
        return "LineTool";
    }
}
