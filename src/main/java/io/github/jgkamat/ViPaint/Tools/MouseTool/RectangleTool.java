package io.github.jgkamat.ViPaint.Tools.MouseTool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 * A solid rectangle tool drawing class
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class RectangleTool implements MouseTool {

    private double startx, starty;

    @Override
    public void onPress(MouseEvent e, GraphicsContext g) {
        startx = e.getX();
        starty = e.getY();
    }

    @Override
    public void onDrag(MouseEvent e, GraphicsContext g) {
        if (e.getY() < starty && e.getX() < startx) {
            g.fillRect(e.getX(), e.getY(), startx - e.getX(), starty
                - e.getY());
        } else if (e.getY() < starty) {
            g.fillRect(startx, e.getY(), e.getX() - startx, starty - e.getY());
        } else if (e.getX() < startx) {
            g.fillRect(e.getX(), starty, startx - e.getX(), e.getY() - starty);
        } else {
            g.fillRect(startx, starty, e.getX() - startx, e.getY() - starty);
        }
    }

    @Override
    public void onRelease(MouseEvent e, GraphicsContext g) {
        onDrag(e, g);
    }

    @Override
    public String getName() {
        return "RectangleTool";
    }
}
