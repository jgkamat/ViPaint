package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import io.github.jgkamat.ViPaint.Canvas.ForegroundCanvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * A class that represents a cursor in vim. A single cursor should
 * exist per program, and will be shared across the program.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Cursor {

    // starting width/location of cursor
    public static final double START_CURSOR_WIDTH = 10, START_CURSOR_HEIGHT =
        10, START_CURSOR_X = 2, START_CURSOR_Y = 2, BORDER_RATIO = 10;

    private ForegroundCanvas border;

    private double x, y, width, height;

    /**
     * Creates a cursor
     */
    public Cursor(ForegroundCanvas border) {
        x = START_CURSOR_X;
        y = START_CURSOR_Y;
        width = START_CURSOR_WIDTH;
        height = START_CURSOR_HEIGHT;
        this.border = border;

        updateStroke();
    }

    /**
     * Creates a NICE cursor
     * @param x X pos
     * @param y y pos
     * @param width width
     * @param height height
     */
    public Cursor(double x, double y, double width, double height,
                  ForegroundCanvas border) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.border = border;
    }

    /**
     * Updates stroke in all canvases
     */
    private void updateStroke() {
        if (border != null) {
            border.getGraphicsContext2D().setLineWidth(getStroke());
            if (border.getLowerWindow() != null) {
                border.getLowerWindow()
                    .getGraphicsContext2D()
                    .setLineWidth(getStroke());
            }
        }
    }

    /**
     * Gets the height of the cursor
     * @return height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets the height of the cursor
     * @param height the height to set
     */
    public void setHeight(double height) {
        if (height <= 0) {
            this.height = 1;
            return;
        }
        this.height = height;

        updateStroke();
    }

    /**
     * Gets the x position of the cursor
     * @return the x position of the top left point
     */
    public double getX() {
        return x;
    }

    /**
     * Sets the location of the cursor
     * @param x the location to move
     * @param y The location to move
     */
    public void set(double x, double y) {
        this.x = x;
        this.y = y;
        boundsCheck();
    }

    /**
     * If the cursor is out of bounds, move it back into the canvas
     */
    public void boundsCheck() {
        //just like collision detection... it actually is collision detection.
        if (this.getY() + this.getHeight() <= 0) {
            this.setY(-this.getHeight() + 1);
        }
        if (this.getY() >= border.getHeight()) {
            this.setY(border.getHeight() - 1);
        }
        if (this.getX() + this.getWidth() <= 0) {
            this.setX(-this.getWidth() + 1);
        }
        if(this.getX() >= border.getWidth()) {
            this.setX(border.getWidth() - 1);
        }
    }

    /**
     * Sets the top left x value
     * @param x value to set
     */
    public void setX(double x) {
        set(x, this.getY());
    }

    /**
     * Gets the y value of the top left point
     * @return the value to set
     */
    public double getY() {
        return y;
    }

    /**
     * Sets the y value of the top left point
     * @param y the value to set.
     */
    public void setY(double y) {
        set(this.getX(), y);
    }

    /**
     * Gets the center x value
     * @return center x
     */
    public double getCenterX() {
        return x + width / 2;
    }

    /**
     * Gets the center y value
     * @return the center y value
     */
    public double getCenterY() {
        return y + height / 2;
    }

    /**
     * Sets the center x of this cursor
     * @param toSet The x to set
     */
    public void setCenterX(double toSet) {
        setX(toSet - this.getWidth() / 2.0);
    }

    /**
     * Sets the center y of this cursor
     * @param toSet The y to set
     */
    public void setCenterY(double toSet) {
        setY(toSet - this.getHeight() / 2.0);
    }

    /**
     * Sets both x and y at the same time
     * @param xSet new center x
     * @param ySet new center y
     */
    public void setCenter(double xSet, double ySet) {
        setCenterX(xSet);
        setCenterY(ySet);
    }

    /**
     * Shrinks this cursor but keeps center in place
     * @param amt amount to shrink by
     */
    public void shrink(double amt) {
        double centX = getCenterX();
        double centY = getCenterY();

        setHeight(getHeight() - amt);
        setWidth(getWidth() - amt);

        setCenter(centX, centY);
    }

    /**
     * Shrinks this cursor but keeps center in place
     * @param amt amount to shrink by
     */
    public void grow(double amt) {
        double centX = getCenterX();
        double centY = getCenterY();

        setHeight(getHeight() + amt);
        setWidth(getWidth() + amt);

        setCenter(centX, centY);
    }

    /**
     * Gets the width of the cursor
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * Gets the 'current stroke width' of this cursor.
     * @return Stroke width
     */
    public double getStroke() {
        return (width + height) / 2.0;
    }

    /**
     * Sets the width of the cursor
     * @param width the Width to set
     */
    public void setWidth(double width) {
        if (width <= 0) {
            this.width = 1;
            return;
        }
        this.width = width;

        updateStroke();
    }

    public void paint(GraphicsContext g) {
        Paint oldF = g.getFill();
        Paint oldS = g.getStroke();
        double oldThick = g.getLineWidth();

        g.setFill(Color.YELLOW);
        g.setStroke(Color.RED);
        g.setLineWidth(((getHeight() + getWidth()) / 2) / BORDER_RATIO);

        g.fillRect(this.getX(), this.getY(), this.getWidth(),
                this.getHeight());
        g.strokeRect(this.getX(), this.getY(), this.getWidth(),
                this.getHeight());

        g.setLineWidth(oldThick);
        g.setFill(oldF);
        g.setStroke(oldS);
    }

    /**
     * Moves the cursor up
     * @param amt the amount to move up
     */
    public void moveUp(double amt) {
        set(this.getX(), this.getY() - amt);
    }

    /**
     * Moves the cursor down
     * @param amt the amount to move down
     */
    public void moveDown(double amt) {
        set(this.getX(), this.getY() + amt);
    }

    /**
     * Moves the cursor left
     * @param amt the amount to move left
     */
    public void moveLeft(double amt) {
        set(this.getX() - amt, this.getY());
    }

    /**
     * Moves the cursor right
     * @param amt the amount to move right
     */
    public void moveRight(double amt) {
        set(this.getX() + amt, this.getY());
    }

}
