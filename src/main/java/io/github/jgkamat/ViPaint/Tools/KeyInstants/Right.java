package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Handlers.SettingManager;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

import java.util.HashSet;
import java.util.Set;

/**
 * A right tool
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Right implements KeyInstant {
    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        c.moveRight(((int) SettingManager.get("move_distance")) * repetitions);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.L;
    }

    @Override
    public Set<KeyCode> getDefaultKeys() {
        HashSet<KeyCode> ans = new HashSet<>();
        ans.add(getDefaultKey());
        ans.add(KeyCode.RIGHT);
        return ans;
    }

    @Override
    public String getName() {
        return "Right";
    }

    @Override
    public String getHelp() {
        return "The Right instant tool will move the cursor right by 'move_amount'";
    }

}
