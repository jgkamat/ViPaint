package io.github.jgkamat.ViPaint.Tools.MouseTool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 * A mousetools interface
 */
public interface MouseTool {

    /**
     * Tools method that is called when the mouse is pressed.
     * Usually involves beginning drawing something.
     *
     * @param e The mouseevent that fired this onPress.
     * @param g The current graphics context.
     */
    void onPress(MouseEvent e, GraphicsContext g);

    /**
     * Tools method that is called when the mouse is dragged.
     * Usually involves updating the drawing location.
     *
     * @param e The mouseevent that fired this onDrag.
     * @param g The current graphics context.
     */
    void onDrag(MouseEvent e, GraphicsContext g);

    /**
     * Tools method that is called when the mouse is released.
     * Usually involves completing an individual stroke/shape.
     *
     * @param e The mouseevent that fired this onRelease.
     * @param g The current graphics context.
     */
    void onRelease(MouseEvent e, GraphicsContext g);

    /**
     * The name of this tool.
     *
     * @return This tool's name.
     */
    String getName();
}
