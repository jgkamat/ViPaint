package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * A key dropper/choose color tool
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Choose implements KeyInstant {

    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        WritableImage writableImage =
                new WritableImage((int) Math.round(g.getCanvas().getWidth()),
                        (int) Math.round(g.getCanvas().getHeight()));
        g.getCanvas().snapshot(null, writableImage);

        Color out = writableImage.getPixelReader().getColor((int) Math.round(c.getX()), (int) Math.round(c.getY()));

        g.setFill(out);
        g.setStroke(out);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.C;
    }

    @Override
    public String getName() {
        return "Choose";
    }

    @Override
    public String getHelp() {
        return "The choose tool is used for selecting existing colors.\n\n" +
            "Press c to activate the tool, the color at the upper left\n" +
            "of the cursor will be used as the new color.";
    }

}
