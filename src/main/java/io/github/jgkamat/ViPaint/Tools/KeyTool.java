package io.github.jgkamat.ViPaint.Tools;

import javafx.scene.input.KeyCode;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This interface represents a KeyTool object.
 * KeyTools are split into KeyModeTools and KeyInstants
 *
 * @author Jay Kamat
 * @version 1.0
 */
public interface KeyTool {
    /**
     * The primary keycode of this tool.
     *
     * @return This tool's VK value
     */
    KeyCode getDefaultKey();

    /**
     * Gets the default keys of this tool. By default, this just returns a single default key
     *
     * @return A set of keyCodes that are bound on start to this tool
     */
    default Set<KeyCode> getDefaultKeys() {
        HashSet<KeyCode> ans = new HashSet<>();
        ans.add(getDefaultKey());
        return ans;
    }


    /**
     * The name of the string
     *
     * @return the tools name
     */
    String getName();

    /**
     * Gets the help text for the specified class
     * @return The help text
     */
    default String getHelp() {
        return "The help text for this class has not been created yet!";
    }
}
