package io.github.jgkamat.ViPaint.Tools.KeyModeTools;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Rectangle;

/**
 * A ViPaint class that represents a Triangle!
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Triangle implements KeyModeTool {
    private double startx, starty;

    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        startx = c.getCenterX();
        starty = c.getCenterY();
        g.setLineWidth(c.getStroke());
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        if(e.isUppercase()) {
            g.fillPolygon(
                new double[] {startx,
                    c.getCenterX(),
                    startx + (c.getCenterX() - startx) / 2.0},

                new double[] {starty,
                    starty,
                    c.getCenterY()},
                3);

        } else {
            g.strokePolygon(
                new double[] {startx,
                    c.getCenterX(),
                    startx + (c.getCenterX() - startx) / 2.0},

                new double[] {starty,
                    starty,
                    c.getCenterY()},
                3);
        }
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        onMoveInMode(c, e, g);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.T;
    }

    @Override
    public String getName() {
        return "Triangle";
    }

    @Override
    public boolean hasPreview() {
        return true;
    }

    @Override
    public String getHelp() {
        return "The Triangle tool is used to draw filled and sketched "
            + "Rectangles.\n\nPress T for a filled Triangle, and r for a hollow "
            + "one.\nThen move around with h,j,k, and l, and press <ESC> to "
            + "complete the drawing.";
    }
}
