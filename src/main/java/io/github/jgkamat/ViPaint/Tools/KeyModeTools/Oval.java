package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * A simple class that draws ovals
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Oval implements KeyModeTool {

    private double startx, starty;

    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        if(e.isLowercase()) {
            startx = c.getCenterX();
            starty = c.getCenterY();
        } else {
            startx = c.getX();
            starty = c.getY();
        }

        g.setLineWidth(c.getStroke());
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        if(e.isLowercase()) {
            g.strokeOval(Math.min(startx, c.getCenterX()),
                    Math.min(starty, c.getCenterY()),
                    Math.abs(startx - c.getCenterX()),
                    Math.abs(starty - c.getCenterY()));
        } else {
            g.fillOval(Math.min(startx, c.getX()),
                    Math.min(starty, c.getY()),
                    Math.max(startx + c.getWidth() - c.getX(), c.getX() +
                        c.getWidth() - startx),
                    Math.max(starty + c.getHeight() - c.getY(), c.getY() +
                        c.getHeight() - starty));
        }
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        onMoveInMode(c, e, g);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.O;
    }

    @Override
    public String getName() {
        return "Oval";
    }

    @Override
    public boolean hasPreview() {
        return true;
    }

    @Override
    public String getHelp() {
        return "The Oval tool is used to draw filled and sketched Ovals.\n\n" +
                "Press O for a filled oval, and o for a hollow one.\n" +
                " Then move around" +
                "with h,j,k, and l, and press <ESC> to complete the drawing.";
    }
}
