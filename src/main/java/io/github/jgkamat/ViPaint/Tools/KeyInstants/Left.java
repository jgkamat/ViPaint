package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Handlers.SettingManager;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

import java.util.HashSet;
import java.util.Set;

/**
 * A left Tool.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Left implements KeyInstant {
    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        c.moveLeft(((int) SettingManager.get("move_distance")) * repetitions);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.H;
    }

    @Override
    public Set<KeyCode> getDefaultKeys() {
        HashSet<KeyCode> ans = new HashSet<>();
        ans.add(getDefaultKey());
        ans.add(KeyCode.LEFT);
        return ans;
    }

    @Override
    public String getName() {
        return "Left";
    }

    @Override
    public String getHelp() {
        return "The Left instant tool will move the cursor left by 'move_amount'";
    }

}
