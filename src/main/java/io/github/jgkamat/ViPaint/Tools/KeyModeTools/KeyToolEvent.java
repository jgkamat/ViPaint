package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.input.KeyEvent;

/**
 * This class will be passed to keytools to give them information about the
 * keypress.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class KeyToolEvent {

    private KeyEvent key;

    public KeyToolEvent(KeyEvent event) {
        this.key = event;
    }

    public char getKeyChar() {
        if (Character.isLetterOrDigit(key.getText().charAt(0))) {
            return key.getText().charAt(0);
        } else {
            throw new IllegalArgumentException("Key Parsed was not a " +
                    "letter or number!");
        }
    }

    public boolean isUppercase() {
        return Character.isUpperCase(this.getKeyChar());
    }

    public boolean isLowercase() {
        return !isUppercase();
    }
}
