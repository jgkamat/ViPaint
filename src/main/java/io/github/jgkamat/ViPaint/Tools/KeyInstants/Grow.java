package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Handlers.SettingManager;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

import java.util.HashSet;
import java.util.Set;

/**
 * A Down tool
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Grow implements KeyInstant {

    @Override
    public void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions) {
        c.grow(1 * repetitions);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.ADD;
    }

    @Override
    public Set<KeyCode> getDefaultKeys() {
        HashSet<KeyCode> ans = new HashSet<>();
        ans.add(getDefaultKey());
        ans.add(KeyCode.EQUALS);
        ans.add(KeyCode.PLUS);
        return ans;
    }

    @Override
    public String getName() {
        return "Grow";
    }

    @Override
    public String getHelp() {
        return "The Grow instant tool will grow the cursor";
    }

}
