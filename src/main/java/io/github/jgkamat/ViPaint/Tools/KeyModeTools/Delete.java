package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * A delete tool that behaves like an eraser
 * @author Jay Kamat
 * @version 1.0
 */
public class Delete implements KeyModeTool {

    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        g.setLineWidth(c.getStroke());

        onMoveInMode(c, e, g);
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        g.clearRect(c.getX(), c.getY(), c.getWidth(), c.getHeight());
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        onMoveInMode(c, e, g);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.D;
    }

    @Override
    public String getName() {
        return "Delete";
    }

    @Override
    public boolean hasPreview() {
        return false;
    }

    @Override
    public String getHelp() {
        return "The delete tool is used for deleting a painted over section.\n"
            + "\nPress D or d to activate the tool, move around\nwith h,j,k, "
            + "and l, and press <ESC> to complete the drawing.";
    }
}
