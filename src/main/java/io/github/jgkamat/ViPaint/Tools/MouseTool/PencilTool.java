package io.github.jgkamat.ViPaint.Tools.MouseTool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 * A simple pencil tool!
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class PencilTool implements MouseTool {

    private double lastx = -1, lasty = -1;

    @Override
    public void onPress(MouseEvent e, GraphicsContext g) {
        onDrag(e, g);
    }

    @Override
    public void onDrag(MouseEvent e, GraphicsContext g) {
        if (lastx == -1 || lasty == -1) {
            lastx = e.getX();
            lasty = e.getY();
        }

        g.strokeLine(lastx, lasty, e.getX(), e.getY());

        lastx = e.getX();
        lasty = e.getY();
    }

    @Override
    public void onRelease(MouseEvent e, GraphicsContext g) {
        onDrag(e, g);

        lastx = -1;
        lasty = -1;
    }

    @Override
    public String getName() {
        return "PencilTool";
    }
}
