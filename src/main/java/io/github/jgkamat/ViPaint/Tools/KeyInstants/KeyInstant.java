package io.github.jgkamat.ViPaint.Tools.KeyInstants;

import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;

import io.github.jgkamat.ViPaint.Tools.KeyTool;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * This interface represents a KeyInstant Object.
 * KeyInstant tools are single use tools like the moving tool.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public interface KeyInstant extends KeyTool {

    /**
     * Tools method that is called when the key is pushed.
     *
     * @param c The cursor that is passed in.
     * @param g The current graphics context.
     * @param e The keyToolEvent that is passed in.
     * @param repetitions the number of times to run this KeyInstant. Will be > 0.
     *
     */
    void onPress(Cursor c, KeyToolEvent e, GraphicsContext g, int repetitions);

}
