package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * A line tool for ViPaint
 * @author Jay Kamat
 * @version 1.0
 */
public class Line implements KeyModeTool {

    private double startx, starty;

    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        startx = c.getCenterX();
        starty = c.getCenterY();
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        g.strokeLine(startx, starty, c.getCenterX(), c.getCenterY());
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        onMoveInMode(c, e, g);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.N;
    }

    @Override
    public String getName() {
        return "Line";
    }

    @Override
    public String getHelp() {
        return "The Line tool is used to draw perfectly straight lines.\n\n" +
                "Press N or n to activate the tool, move around\n" +
                "with h,j,k, and l, and press <ESC> to complete the drawing.";
    }

    @Override
    public boolean hasPreview() {
        return true;
    }
}
