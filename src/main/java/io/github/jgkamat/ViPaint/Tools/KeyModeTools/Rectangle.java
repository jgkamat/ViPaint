package io.github.jgkamat.ViPaint.Tools.KeyModeTools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

/**
 * A ViPaint class that represents a rectangle!
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class Rectangle implements KeyModeTool {
    private double startx, starty;

    @Override
    public void onEnterMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
       if(e.isUppercase()) {
           startx = c.getX();
           starty = c.getY();
       } else {
           startx = c.getCenterX();
           starty = c.getCenterY();
       }

        g.setLineWidth(c.getStroke());
    }

    @Override
    public void onMoveInMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        if(e.isUppercase()) {
            g.fillRect(Math.min(startx, c.getX()),
                    Math.min(starty, c.getY()),
                    Math.max(startx + c.getWidth() - c.getX(), c.getX() +
                        c.getWidth() - startx),
                    Math.max(starty + c.getHeight() - c.getY(), c.getY() +
                        c.getHeight() - starty));
        } else {
            g.strokeRect(Math.min(startx, c.getCenterX()),
                    Math.min(starty, c.getCenterY()),
                    Math.abs(startx - c.getCenterX()),
                    Math.abs(starty - c.getCenterY()));
        }
    }

    @Override
    public void onExitMode(Cursor c, KeyToolEvent e, GraphicsContext g) {
        onMoveInMode(c, e, g);
    }

    @Override
    public KeyCode getDefaultKey() {
        return KeyCode.R;
    }

    @Override
    public String getName() {
        return "Rectangle";
    }

    @Override
    public boolean hasPreview() {
        return true;
    }

    @Override
    public String getHelp() {
        return "The Rectangle tool is used to draw filled and sketched "
            + "Rectangles.\n\nPress R for a filled rectangle, and r for a hollow "
            + "one.\nThen move around with h,j,k, and l, and press <ESC> to "
            + "complete the drawing.";
    }
}
