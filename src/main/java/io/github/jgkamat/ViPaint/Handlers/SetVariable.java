package io.github.jgkamat.ViPaint.Handlers;

import io.github.jgkamat.ViPaint.VimBar.CommandManager;

import java.util.function.Function;

/**
 * A class to store the state of a set variable, and provide a 'hook'
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class SetVariable {
    private boolean state;
    private Function<CommandManager.PropertyHolder, Boolean> setHook, unSetHook;
    private CommandManager.PropertyHolder propHolder;

    public SetVariable(boolean state, CommandManager.PropertyHolder holder,
                       Function<CommandManager.PropertyHolder, Boolean> setHook,
                       Function<CommandManager.PropertyHolder, Boolean> unSetHook) {
        this.state = state;
        this.setHook = setHook;
        this.propHolder = holder;
        this.unSetHook = unSetHook;
    }

    /**
     * Gets the state of this property holder
     * @return The current boolean state
     */
    public boolean getState() {
        return state;
    }

    /**
     * Sets this SetVar to true
     */
    public void set() {
        setHook.apply(propHolder);
        this.state = true;
    }

    /**
     * Unsets this setVar
     */
    public void unSet() {
       unSetHook.apply(propHolder);
        this.state = false;
    }

    /**
     * Toggles the state of this setVar
     */
    public void toggle() {
        if (getState()) {
            unSet();
        } else {
            set();
        }
    }

}
