package io.github.jgkamat.ViPaint.Handlers;

import io.github.jgkamat.ViPaint.Tools.KeyTool;
import javafx.scene.input.KeyCode;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * A class to manage and store keybindings
 *
 * This is where you would change the bindings of keys that
 * link to keytools
 */
public class KeybindManager {

    Map<KeyCode, KeyTool> toolKeyList;
    Map<KeyTool, KeyCode> keyToolList;

    public KeybindManager() {
        toolKeyList = new HashMap<>();
        keyToolList = new HashMap<>();
    }

    public void setDefaultBindings(KeyToolHandler handler) {
        Collection<KeyTool> toAdd = handler.getAllTools();

        for(KeyTool loopy: toAdd) {
            for(KeyCode code: loopy.getDefaultKeys()) {
                this.bind(loopy, code);
            }
        }
    }

    public boolean keyIsBound(KeyTool in) {
        return toolKeyList.containsValue(in) || keyToolList.containsKey(in);
    }

    public void bind(KeyTool toBind, KeyCode key) {
        toolKeyList.put(key, toBind);
        keyToolList.put(toBind, key);
    }

    public void unbind(KeyTool tool) {
        if(tool == null)
            return;

        toolKeyList.remove(keyToolList.get(tool));
        keyToolList.remove(tool);
    }

    public void unbind(KeyCode key) {
        if(key == null)
            return;

        keyToolList.remove(toolKeyList.get(key));
        toolKeyList.remove(key);
    }

    public KeyCode getBinding(KeyTool keyTool) {
        return keyToolList.get(keyTool);
    }

    public KeyTool getTool(KeyCode binding) {
        return toolKeyList.get(binding);
    }

    public boolean isBound(KeyCode key) {
        return toolKeyList.get(key) != null;
    }
}
