package io.github.jgkamat.ViPaint.Handlers;

import io.github.jgkamat.ViPaint.Tools.KeyInstants.KeyInstant;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyModeTool;
import io.github.jgkamat.ViPaint.Tools.KeyTool;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A Toolhandler for keys
 * @author Jay Kamat
 * @version 1.0
 */
public class KeyToolHandler {

    private Map<String, KeyModeTool> nameToolList;
    private Map<String, KeyInstant> instantToolList;
    private KeyModeTool currentTool;

    /**
     * Creates a keytoolhandler
     */
    public KeyToolHandler() {
        nameToolList = new HashMap<>();
        instantToolList = new HashMap<>();
        currentTool = null;
    }

    /**
     * Gets the list of current tools
     * @return the current list of tools
     */
    public Collection<KeyTool> getAllTools() {
        HashSet<KeyTool> toGet = new HashSet<>(nameToolList.values());
        toGet.addAll(instantToolList.values());
        return toGet;
    }

    public Collection<KeyModeTool> getKeyModeTools() {
        return nameToolList.values();
    }

    public Collection<KeyInstant> getKeyInstantTools() {
        return instantToolList.values();
    }

    public Collection<Class<? extends KeyModeTool>> getClasses() {
        return nameToolList.values().stream()
                .map((KeyModeTool tool) -> tool.getClass())
                .collect(Collectors.toList());
    }

    /**
     * Adds a tool to this toolhandler
     * @param toAdd The keytool to add
     */
    public void addTool(KeyModeTool toAdd) {
        nameToolList.put(toAdd.getName(), toAdd);
    }

    /**
     * Adds a tool to this toolhandler
     * @param toAdd The keytool to add
     */
    public void addTool(KeyInstant toAdd) {
        instantToolList.put(toAdd.getName(), toAdd);
    }

    /**
     * Sets this keytool to a tool
     *
     * @param in the tool to set
     * @return Whether this set was successful
     */
    public boolean setTool(KeyModeTool in) {
        if (nameToolList.containsValue(in)) {
            currentTool = in;
            return true;
        }
        return false;
    }

    /**
     * Sets a tool by name
     * @param in The string to look for
     * @return success
     */
    public boolean setTool(String in) {
        if(nameToolList.containsKey(in)) {
            currentTool = nameToolList.get(in);
        }
        return false;
    }

    /**
     * Checks to see if the tool is contained
     * @param key the search key
     * @return Found
     */
    public boolean containsToolName(String key) {
        return nameToolList.containsKey(key);
    }

    /**
     * Gets values from the handler
     * @param key the key to search for
     * @return the found tool
     */
    public KeyModeTool getModeTool(String key) {
        return nameToolList.get(key);
    }

    /**
     * Gets the current tool in the handler
     * @return the current tool
     */
    public KeyModeTool getCurrentTool() {
        return currentTool;
    }

}
