package io.github.jgkamat.ViPaint.Handlers;

import io.github.jgkamat.ViPaint.Tools.MouseTool.MouseTool;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages tools!
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class MouseToolHandler {

    private Map<String, MouseTool> toolList;
    private String currentTool;

    /**
     * Constructs a toolhandler
     */
    public MouseToolHandler() {
        toolList = new HashMap<String, MouseTool>();
        currentTool = null;
    }

    /**
     * Adds a tool to this toolhandler
     *
     * @param toAdd The tool to add
     */
    public void addTool(MouseTool toAdd) {
        toolList.put(toAdd.getName(), toAdd);
    }

    /**
     * Sets the current graphical tool in the toolhandler
     *
     * @param in The tool to switch to
     */
    public void setTool(String in) {
        currentTool = in;
    }

    /**
     * Gets the currently selected tool in the handler
     *
     * @return The currently selected tool
     */
    public MouseTool getCurrentTool() {
        return toolList.get(currentTool);
    }

}
