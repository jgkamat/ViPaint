package io.github.jgkamat.ViPaint.Handlers;

import javafx.scene.paint.Color;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A class stores settings and constants with a public Map.
 * This is very bad OOP, but it makes everything easy.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class SettingManager {
    public static Map<String, Object> mappings;

    static {
        mappings = new HashMap<>();

        // all mappings for 'let'
        mappings.put("move_distance", 5);
        mappings.put("color", Color.BLACK);
        mappings.put("rawthickness", 10);

        // Boolean values are added in CommandManager, under the let tool.
    }

    /**
     * A simple wrapper for the map
     * @param key The Key to find
     * @return The object that matches the key
     */
    public static Object get(String key) {
        return mappings.get(key);
    }

    /**
     * A simple wrapper for the map
     * @param key The key to add
     * @param value The object to pair with the key
     */
    public static void put(String key, Object value) {
        mappings.put(key, value);
    }

    /**
     * Return the set of keys
     * @return keys
     */
    public static Set<String> keySet() {
        return mappings.keySet();
    }

    /**
     * Returns the entryset
     * @return The entries
     */
    public static Set<Map.Entry<String, Object>> entrySet() {
        return mappings.entrySet();
    }

    /**
     * Returns a collections of the values
     * @return A collection of the values
     */
    public static Collection<Object> values() {
        return mappings.values();
    }

    public static Map<String, Object> getClone() {
        return new HashMap<>(mappings);
    }

    /**
     * The amount of mappings stored
     * @return The size of the map
     */
    public static int size() {
        return mappings.size();
    }
}
