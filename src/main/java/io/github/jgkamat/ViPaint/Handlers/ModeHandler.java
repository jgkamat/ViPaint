package io.github.jgkamat.ViPaint.Handlers;

import io.github.jgkamat.ViPaint.Tools.KeyInstants.KeyInstant;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyModeTool;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.KeyToolEvent;
import io.github.jgkamat.ViPaint.VimBar.VimBar;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * This class handles what mode is currently selected and movement.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class ModeHandler {
    // grow and shrink
    private KeyToolHandler handler;
    private Cursor cursor;
    private VimBar vimBar;
    private KeybindManager bindings;
    private KeyToolEvent currentEvent;
    private int repeatNum = 0;

    /**
     * Creates a modehandler
     *
     * @param handler A keytool handler to use
     * @param vimbar The vimbar to output to
     * @param bindings The bindings object to interface with
     * @param cursor The cursor to modify/use
     */
    public ModeHandler(KeyToolHandler handler, VimBar vimbar,
                       KeybindManager bindings, Cursor cursor) {
        this.handler = handler;
        this.cursor = cursor;
        this.vimBar = vimbar;
        this.bindings = bindings;
    }

    /**
     * Filters KeyEvents by what they do!
     *
     * @param input     The input keyevent
     * @param primary   the graphicscontext for drawing
     * @param secondary The graphicscontext for preview
     * @return to refresh the screen or not
     */
    public boolean keyIn(KeyEvent input, GraphicsContext primary,
                         GraphicsContext secondary) {
        secondary.setStroke(primary.getStroke());
        secondary.setFill(primary.getFill());
        secondary.setLineWidth(primary.getLineWidth());

        GraphicsContext toDraw = primary;

        int repeatNum = this.repeatNum;

        if (input.getCode().isDigitKey()) {
            this.repeatNum = this.repeatNum * 10 + Integer.parseInt(input.getText());
            repeatNum = this.repeatNum;
            return false;
        } else {
            this.repeatNum = 0;
        }

        if (handler.getCurrentTool().getName().equalsIgnoreCase("Normal")
            && (input.getCode() == KeyCode.COLON
                || input.getCode() == KeyCode.SEMICOLON)) {
            vimBar.enable();
            return false;
        }

        if (input.getCode() == null) {
            System.err.println("Unknown Error!");
            return true;
        }

        if (input.getCode() == bindings.getBinding(handler.getModeTool("Normal"))) {
            secondary.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
            handler.getCurrentTool().onExitMode(cursor,
                    currentEvent, toDraw);
            handler.setTool(handler.getModeTool("Normal"));
            vimBar.setText("--"
                + handler.getCurrentTool().getName().toUpperCase() + "--");
        }

        if (handler.getCurrentTool().hasPreview()) {
            toDraw = secondary;
        }

        if (bindings.getTool(input.getCode()) instanceof KeyInstant) {

            ((KeyInstant) bindings.getTool(input.getCode()))
                .onPress(cursor, currentEvent, toDraw, Math.max(1, repeatNum));

            secondary.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
            handler.getCurrentTool().onMoveInMode(cursor,
                    currentEvent, toDraw);

        } else if (handler.getCurrentTool().getName().equals("Normal")) {
            if (bindings.isBound(input.getCode())
                && bindings.getTool(input.getCode()) instanceof KeyModeTool) {
                handler.setTool((KeyModeTool) bindings.getTool(input.getCode()));
                currentEvent = new KeyToolEvent(input);
                handler.getCurrentTool().onEnterMode(cursor,
                        currentEvent, toDraw);
                vimBar.setText("--"
                    + handler.getCurrentTool().getName().toUpperCase() + "--");
            } else {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}
