package io.github.jgkamat.ViPaint.Canvas;

import io.github.jgkamat.ViPaint.Handlers.MouseToolHandler;
import io.github.jgkamat.ViPaint.Tools.MouseTool.PencilTool;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 * This is a temporary canvas for displaying in-progress changes. For example,
 * the click-and-drag effect when using a rectangle tool before the mouse is
 * released. Permanent changes should be sent to PaintCanvas.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class ForegroundCanvas extends Canvas {

    private MouseToolHandler handler;
    private GraphicsContext g;
    private Canvas lowerWindow;

    /**
     * A canvas that does previews
     * @param width The width
     * @param height Height
     * @param handler A toolhandler object
     * @param lowerWindow The lower canvas
     */
    public ForegroundCanvas(int width, int height, MouseToolHandler handler,
            Canvas lowerWindow) {
        super(width, height);
        this.handler = handler;
        g = this.getGraphicsContext2D();
        this.lowerWindow = lowerWindow;
        this.setFocusTraversable(false);

        this.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        lowerWindow.fireEvent(event);
                    }
                });

        this.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        //the pencil needs to preserve its drag movements!
                        if (handler.getCurrentTool() instanceof PencilTool) {
                            lowerWindow.fireEvent(event);
                        } else {
                            g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
                            handler.getCurrentTool().onDrag(event, g);
                        }
                    }
                });

        this.addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        //clear the temp thingy
                        g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
                        lowerWindow.fireEvent(event);
                    }
                });
    }

    /**
     * Clears the entire canvas.
     */
    public void clear() {
        this.g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
    }

    /**
     * Gets the canvas below this one.
     * @return The canvas below this one
     */
    public Canvas getLowerWindow() {
        return lowerWindow;
    }
}
