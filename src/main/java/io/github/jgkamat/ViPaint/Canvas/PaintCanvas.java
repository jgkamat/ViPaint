package io.github.jgkamat.ViPaint.Canvas;

import io.github.jgkamat.ViPaint.Handlers.MouseToolHandler;
import io.github.jgkamat.ViPaint.Tools.MouseTool.PencilTool;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 * This is a modified JavaFX Canvas, designed to display the user's permanent
 * changes to the drawing.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class PaintCanvas extends Canvas {

    private GraphicsContext g;

    /**
     * Creates a PaintCanvas.
     * @param width The width of the PaintCanvas
     * @param height The height of the PaintCanvas
     * @param handler A toolhandler to send events to
     */
    public PaintCanvas(int width, int height, MouseToolHandler handler) {
        super(width, height);
        g = this.getGraphicsContext2D();
        this.setFocusTraversable(false);

        this.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        handler.getCurrentTool().onPress(event, g);
                    }
                });

        // This should only be called on a pencilTool
        this.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (handler.getCurrentTool() instanceof PencilTool) {
                            handler.getCurrentTool().onDrag(event, g);
                        }
                    }
                });


        this.addEventHandler(MouseEvent.MOUSE_RELEASED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        handler.getCurrentTool().onRelease(event, g);
                    }
                });
    }

    public void clear() {
        this.getGraphicsContext2D().clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
    }
}
