package io.github.jgkamat.ViPaint.Canvas;

import io.github.jgkamat.ViPaint.Handlers.ModeHandler;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.Cursor;
import io.github.jgkamat.ViPaint.VimBar.VimBar;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * This class is a modified JavaFX Canvas, designed to display the Vim cursor.
 *
 * @author Jay Kamat
 * @version 1.0
 */
public class CursorCanvas extends Canvas {

    private GraphicsContext g;
    private Cursor cursor;
    private boolean helpOn;
    private ForegroundCanvas lowerWindow;

    /**
     * Creates a cursor Canvas
     *
     * @param width       Width of canvas
     * @param height      Height of canvas
     * @param lowerWindow A lower window
     * @param cursor      A keycursor
     * @param modeHandler A modehandler
     */
    public CursorCanvas(int width, int height,
                        ForegroundCanvas lowerWindow,
                        Cursor cursor, ModeHandler modeHandler,
                        VimBar vimBar) {
        super(width, height);
        this.lowerWindow = lowerWindow;
        g = this.getGraphicsContext2D();
        this.cursor = cursor;

        this.addEventFilter(MouseEvent.MOUSE_PRESSED, (e) -> {
            this.requestFocus();
            vimBar.disable();
        });
        this.setFocusTraversable(true);
        this.requestFocus();
        helpOn = true;

        this.addEventHandler(KeyEvent.KEY_PRESSED,
            event -> {
                boolean refresh = modeHandler.keyIn(event,
                    lowerWindow.getLowerWindow().getGraphicsContext2D(),
                    lowerWindow.getGraphicsContext2D());
                if (!refresh) {
                    return;
                }

                helpOn = false;

                paintCursor();

                // System.out.println(event.getCode().
                // getName() +" Key Detected");
            });

        this.addEventHandler(MouseEvent.ANY,
            event -> {
                if (event.getButton() != MouseButton.NONE) {
                    helpOn = false;
                    g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
                }
                paintCursor();
                lowerWindow.fireEvent(event);
            });
        this.widthProperty().addListener((observableValue, oldSceneHeight,
                                          newSceneHeight) -> {
            paintCursor();
            cursor.boundsCheck();
        });

        this.heightProperty().addListener((observableValue, oldSceneHeight,
                                           newSceneHeight) -> {
            paintCursor();
            cursor.boundsCheck();
        });
    }

    private void paintCursor() {
        if (!helpOn) {
            g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
        }

        cursor.paint(g);
    }

    /**
     * Clears this canvas
     */
    public void clear() {
        g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);
    }

    /**
     * This turns on some stuff that handles the help/intro dialogue.
     */
    public void setHelpOn() {
        helpOn = true;
    }

    /**
     * Gets the canvas below this one.
     * @return The canvas below this one
     */
    public ForegroundCanvas getLowerWindow() {
        return lowerWindow;
    }
}
