package io.github.jgkamat.ViPaint;

import io.github.jgkamat.ViPaint.Canvas.*;
import io.github.jgkamat.ViPaint.Handlers.*;
import io.github.jgkamat.ViPaint.Tools.KeyInstants.*;
import io.github.jgkamat.ViPaint.Tools.KeyModeTools.*;
import io.github.jgkamat.ViPaint.Tools.KeyTool;
import io.github.jgkamat.ViPaint.Tools.MouseTool.*;
import io.github.jgkamat.ViPaint.VimBar.CommandManager;
import io.github.jgkamat.ViPaint.VimBar.VimBar;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Collection;

/**
 * A Paint program for vi and vim addicts!
 * Help is needed in creating Vi[m]Paint!
 *
 * @author Jay Kamat
 * @version 0.1.0
 */
public class ViPaint extends Application {

    public static String version = "0.1.0";

    private MouseToolHandler handler;
    private KeyToolHandler keyHandler;
    private VimBar viTextField;
    private PaintCanvas paint;
    private CursorCanvas cursorCanvas;
    private ForegroundCanvas foreground;
    private KeybindManager bindings;
    private VBox buttonBox;
    private HBox fullWindow;
    private Stage stage;
    private VBox vimBox;

    /**
     * Creates a ViPaint object
     */
    public ViPaint() {
        keyHandler = new KeyToolHandler();
        handler = new MouseToolHandler();
        paint = new PaintCanvas(500, 500, this.handler);
        foreground = new ForegroundCanvas(500, 500, this.handler, paint);

        viTextField = new VimBar();
        bindings = new KeybindManager();

        Cursor cursor = new Cursor(foreground);

        cursorCanvas = new CursorCanvas(500, 500, foreground, cursor, new
                ModeHandler(keyHandler, viTextField, bindings, cursor),
                viTextField);

        viTextField.setCommandManager(new CommandManager(cursorCanvas, paint,
                    this, viTextField));
        viTextField.setDisable(true);

    }

    /**
     * Normally, the entry point is the start method, but main may be called
     * instead in misconfigured clients. If so, call launch to launch properly.
     *
     * @param args Command-line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        // <3
        viTextField.setText("--NORMAL--");

        // paint.getGraphicsContext2D().setLineWidth(2);
        // foreground.getGraphicsContext2D().setLineWidth(2);

        // Set up buttons
        Button pencilButton = new Button();
        pencilButton.setText("Pencil");
        Button lineButton = new Button();
        lineButton.setText("Line");
        Button rectButton = new Button();
        rectButton.setText("Rectangle");
        Button ovalButton = new Button();
        ovalButton.setText("Oval");
        Button clearButton = new Button();
        clearButton.setText("Clear");

        // Dont abbreviate text in rectangle button
        rectButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);

        ColorPicker cpicker = new ColorPicker(Color.BLACK);

        // Send keyevents to the canvas
        pencilButton.setFocusTraversable(false);
        lineButton.setFocusTraversable(false);
        rectButton.setFocusTraversable(false);
        ovalButton.setFocusTraversable(false);
        clearButton.setFocusTraversable(false);
        cpicker.setFocusTraversable(false);

        handler.addTool(new LineTool());
        handler.addTool(new PencilTool());
        handler.addTool(new RectangleTool());
        handler.addTool(new OvalTool());
        handler.setTool("PencilTool");

        keyHandler.addTool(new Normal());
        keyHandler.addTool(new Insert());
        keyHandler.addTool(new Delete());
        keyHandler.addTool(new Rectangle());
        keyHandler.addTool(new Line());
        keyHandler.addTool(new Oval());
        keyHandler.addTool(new Triangle());

        keyHandler.addTool(new Up());
        keyHandler.addTool(new Down());
        keyHandler.addTool(new Left());
        keyHandler.addTool(new Right());
        keyHandler.addTool(new Grow());
        keyHandler.addTool(new Shrink());
        keyHandler.addTool(new Choose());
        keyHandler.addTool(new ToggleMenu());

        keyHandler.setTool("Normal");

        bindings.setDefaultBindings(keyHandler);

        // Configure button actions
        lineButton.setOnAction(e -> handler.setTool("LineTool"));
        pencilButton.setOnAction(e -> handler.setTool("PencilTool"));
        rectButton.setOnAction(e -> handler.setTool("RectangleTool"));
        ovalButton.setOnAction(e -> handler.setTool("OvalTool"));

        clearButton.setOnAction(e ->
                paint.getGraphicsContext2D().clearRect(0, 0,
                        Short.MAX_VALUE,
                        Short.MAX_VALUE));

        cpicker.setOnAction(e -> {
            Color c = cpicker.getValue();
            paint.getGraphicsContext2D().setStroke(c);
            paint.getGraphicsContext2D().setFill(c);
            foreground.getGraphicsContext2D().setFill(c);
        });

        Pane painter = new Pane();
        painter.getChildren().add(paint);
        painter.getChildren().add(foreground);
        painter.getChildren().add(cursorCanvas);
        cursorCanvas.toFront();

        vimBox = new VBox();
        vimBox.getChildren().addAll(painter, viTextField);
        buttonBox = new VBox(10);
        cpicker.setFocusTraversable(false);
        cpicker.setOnAction(event -> cursorCanvas.requestFocus());
        buttonBox.getChildren().addAll(pencilButton, lineButton, rectButton,
            ovalButton, cpicker, clearButton);
        buttonBox.minWidth(1000);
        buttonBox.setFocusTraversable(false);

        fullWindow = new HBox();

        //the button box can be added later (or run showButtons())
        fullWindow.getChildren().addAll(buttonBox, vimBox);
        buttonBox.managedProperty().bind(buttonBox.visibleProperty());
        buttonBox.setVisible(false);

        // Resize canvases automatically on window resize
        paint.widthProperty().bind(fullWindow.widthProperty());
        paint.heightProperty().bind(fullWindow.heightProperty());
        foreground.widthProperty().bind(fullWindow.widthProperty());
        foreground.heightProperty().bind(fullWindow.heightProperty());

        cursorCanvas.widthProperty().bind(fullWindow.widthProperty());
        cursorCanvas.heightProperty().bind(fullWindow.heightProperty());

        stage.setMinWidth(600);
        stage.setMinHeight(500);

        this.stage = stage;

        // make the icon nice =D
		//TODO getModeTool this working again
        stage.getIcons().add(new Image(ViPaint.class.getResourceAsStream("/icon.png")));

        painter.setStyle("-fx-background-color: white");

        Scene scene = new Scene(fullWindow);
        stage.setScene(scene);
        stage.setTitle("ViPaint");
        stage.show();

        // semi-arbitrary amounts
        stage.setMinWidth(20);
        stage.setMinHeight(20);

        cursorCanvas.setHelpOn();
        viTextField.parseCommand("source ~/.viprc");
        viTextField.parseCommand("intro");
    }

    public void toggleButtons(boolean toggle) {
        buttonBox.setVisible(toggle);
    }

    public void toggleButtons() {
        buttonBox.setVisible(!buttonBox.isVisible());
    }

    /**
     * Sets the size of the drawing area
     * @param width width to set
     * @param height height to set
     */
    public void setSize(double width, double height) {
        vimBox.setPrefSize(width, height);
        stage.sizeToScene();
    }

    /**
     * Gets the list of keytools currently loaded into the program
     *
     * @return The list of keytools
     */
    public Collection<KeyTool> getKeyToolClasses() {
        return keyHandler.getAllTools();
    }
}
