package io.github.jgkamat.ViPaint.VimBar;

import javafx.application.Platform;
import javafx.scene.control.TextField;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


/**
 * A text field for use in ViPaint
 * @author Jay Kamat
 * @version 1.0
 */
public class VimBar extends TextField {

    private CommandManager commandManager;

    /**
     * Creates a vimbar
     */
    public VimBar() {
        super();

        this.setOnAction(e -> {
            String text = this.getText();
            if(text.charAt(0) == ':') {
                text = text.substring(1);
            }
            commandManager.parseCommand(text);
            disable();
        });

        this.addEventHandler(KeyEvent.KEY_PRESSED,
                (event) -> {
                    if (event.getCode() == KeyCode.ESCAPE) {
                        disable();
                    } else if (event.getCode() == KeyCode.UP) {
                        setText(":" + commandManager.getPrevious());

                        //positionCaret(getText().length());
                        // a weird workaround, positionCaret dosent work.
                        Platform.runLater(() -> positionCaret(getText().length()));
                    } else if (event.getCode() == KeyCode.DOWN) {
                        setText(":" + commandManager.getNext());
                        //positionCaret(getText().length());
                        // a weird workaround, positionCaret dosent work.
                        Platform.runLater(() -> positionCaret(getText().length()));
                    }
                });

        this.addEventHandler(InputEvent.ANY,
                (event) -> {
                    if(getText().length() <= 0 || getText().charAt(0) != ':') {
                        disable();
                    }
                });
    }


    /**
     * Sets the command manager that the vimBar uses
     * @param commandManager The command manager to set
     */
     public void setCommandManager(CommandManager commandManager) {
        this.commandManager = commandManager;
    }


    public void disable() {
        this.setFocused(false);
        this.setFocusTraversable(false);
        this.setDisable(true);
    }

    public void enable() {
        this.setDisable(false);
        this.setFocusTraversable(true);
        this.requestFocus();
        //shouldnt be needed, as keypress goes directly into the bar!
//        this.setText(":");
    }

    /**
     * Use the embedded parser to parse the given command
     */
    public void parseCommand(String toParse) {
        commandManager.parseCommand(toParse);
    }

}
