package io.github.jgkamat.ViPaint.VimBar;

import io.github.jgkamat.ViPaint.Handlers.SettingManager;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Set;

/**
 * A static class to convert a 'raw' image to a javafx canvas.
 * From there you can save it as a png.
 *
 * @author Jay Kamat
 * @version 0.1
 */
public class RawConverter {

    public static double getWidth(File inFile) throws IOException {
        return Math.ceil(Math.sqrt((int) inFile.length() / 3));
    }

    public static double writeToCanvas(Canvas canvas, File inFile) throws IOException {
        byte[] array = new byte[(int) inFile.length()];
        FileInputStream stream = new FileInputStream(inFile);
        int numbytes = stream.read(array);

        return writeToCanvas(canvas, array);
    }

    public static double writeToCanvas(Canvas canvas, String text) {
        try {
            return writeToCanvas(canvas, text.getBytes("US-ASCII"));
        } catch (UnsupportedEncodingException e) {
            // aaaaaaaaaaah!
            e.printStackTrace();
            System.out.println("Using no encoding?");
            return writeToCanvas(canvas, text.getBytes());
        }
    }

    public static double writeToCanvas(Canvas canvas, byte[] text) {
        // System.out.println(Arrays.toString(text));
        GraphicsContext g = canvas.getGraphicsContext2D();
        Paint oldPaint = g.getFill();
        g.clearRect(0, 0, Short.MAX_VALUE, Short.MAX_VALUE);

        Point upperRight = new Point(0, 0);

        int red = -11;
        int blue = -11;
        int green = -11;

        double width = Math.ceil(Math.sqrt(text.length / 3)) - 1;

        if (width <= 0) {
            throw new IllegalArgumentException("Length could not be calculated properly!");
        }

        int curWidth = 0;

        for (byte toAdd: text) {
            if (red == -11) {
                red = (int) toAdd;
            } else if (green == -11) {
                green = (int) toAdd;
            } else {
                blue = (int) toAdd;

                // System.out.format("red:%4d green:%4d blue:%4d\n", red, green, blue);

                g.setFill(new Color(red / 256.0, green / 256.0, blue / 256.0, 1));
                g.fillRect(upperRight.getX(), upperRight.getY(),
                        (int) SettingManager.get("rawthickness"), (int) SettingManager.get("rawthickness"));

                if (curWidth >= width) {
                    upperRight.setLocation(0, upperRight.getY() + (int) SettingManager.get("rawthickness"));
                    curWidth = 0;
                } else {
                    upperRight.setLocation(upperRight.getX() +
                        (int) SettingManager.get("rawthickness"), upperRight.getY());
                    curWidth++;
                }

                red = -11;
                blue = -11;
                green = -11;
            }
        }
        g.setFill(oldPaint);
        return width;
    }
}
