ViPaint
================

ViPaint is a modal painting program that tries to show how vim-like controls
can be applied to anything!

![ViPaint](vipaint_img.jpg)

#### Gradle
ViPaint uses gradle2+ for builds. To install gradle see [this webpage](https://gradle.org).

Installing gradle on debian is as simple as running one of the following
```shell
apt-get install gradle
```
You must have java 8 installed as well. Be sure ```gradle -v``` gives you a version >2.

#### Compiling
To Compile and run ViPaint, simply run
```shell
gradle run
```
Compiling it without running can be done with
```shell
gradle build
```

#### Modal Interface
Here are a few tips to using ViPaint.

* Type :help for help in the program and a list of (vi-esq) commands
* Some Example commands are
  * I - Insert Mode, a simple pen tool
  * D - Delete Mode, an eraser
  * R - Rectangle Tool. Capital fills the Rectangle
  * O - Oval Tool. Capital fills the Oval
  * N - Line Tool, move to a end point.
* Help pages are available as well. For example, ```:help rectangle``` is available
* Some ex commands are available too. Try:
  * `:let color=blue`
  * `:let move_distance=1`
  * `:w file.png`
  * `:o file.png`
  * `:o file.txt` (this is pretty cool, don't do this with a tiling wm)

If you find a problem, please let me know in issues or mail!


